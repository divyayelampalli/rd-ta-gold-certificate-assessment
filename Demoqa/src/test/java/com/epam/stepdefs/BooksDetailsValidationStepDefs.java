package com.epam.stepdefs;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static io.restassured.RestAssured.given;
import static org.testng.Assert.assertEquals;

import java.util.List;

public class BooksDetailsValidationStepDefs {
	public static final String BOOKSLIST_ENDPOINT = "BookStore/v1/Books";
	Base base;
	
	public BooksDetailsValidationStepDefs(Base base) {
		this.base = base;
	}
	
	@When("the user gets the details of the books")
	public void the_user_gets_the_details_of_the_books() {
		base.response = given()
						.when()
						.get(BOOKSLIST_ENDPOINT);
		base.jsonPath = base.response.jsonPath();
	}

	@When("the user navigates to Books Page")
	public void the_user_navigates_to_books_page() {
		base.getBooksPageObject().openBooksPage();
	}

	@Then("the titles should be same as in the response body")
	public void the_titles_should_be_same_as_in_the_response_body() {
		List<String> titles = base.jsonPath.getList("books.title");
		assertEquals(base.getBooksPageObject().getTilesOfBooksList(), titles);
	}
	
	@Then("the authors should be same as in the response body")
	public void the_authors_should_be_same_as_in_the_response_body() {
		List<String> authors =base.jsonPath.getList("books.author");
		assertEquals(base.getBooksPageObject().getAuthorsOfBooksList(), authors);
		
	}
	
	@Then("the publishers should be same as in the response body")
	public void the_publishers_should_be_same_as_in_the_response_body() {
		List<String> publishers = base.jsonPath.getList("books.publisher");
		assertEquals(base.getBooksPageObject().getPublishersOfBooksList(), publishers);
	}
}
