package com.epam.stepdefs;

public class UserPojo {
	private String username;
	private String password;
	
	public String getUserName() {
		return username;
	}
	public void setUserName(String userName) {
		this.username = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String passwoerd) {
		this.password = passwoerd;
	}
}
