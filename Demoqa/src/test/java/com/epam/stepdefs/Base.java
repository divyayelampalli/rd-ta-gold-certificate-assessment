package com.epam.stepdefs;

import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.epam.driverfactory.DriverManager;
import com.epam.driverfactory.DriverManagerFactory;
import com.epam.pages.BooksPage;
import com.epam.pages.LoginPage;
import com.epam.pages.ProfilePage;

import io.cucumber.java.After;
import io.cucumber.java.AfterAll;
import io.cucumber.java.BeforeAll;
import io.cucumber.java.Scenario;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class Base 
{
	public static final String BASE_URI = "https://demoqa.com/";
	public Response response;
	public JsonPath jsonPath;
	public static WebDriver driver;
	public static DriverManager driverManager;
	public LoginPage loginPage;
	public ProfilePage profilePage;
	public BooksPage booksPage;
	
	@BeforeAll
	public static void setUp() throws IOException {
		RestAssured.baseURI = BASE_URI;
		
		driverManager = DriverManagerFactory.getManager();
		driver = driverManager.getDriver();
	}
	
	public WebDriver getDriver() {
		return driver;
	}
	
	public LoginPage getLoginPageObject() {
		if(loginPage == null) {
			loginPage = new LoginPage(driver);
		}
		return loginPage;
	}
	
	public ProfilePage getProfilePageObject() {
		if(profilePage == null) {
			profilePage = new ProfilePage(driver);
		}
		return profilePage;
	}
	
	public BooksPage getBooksPageObject() {
		if(booksPage == null) {
			booksPage = new BooksPage(driver);
		}
		return booksPage;
	}
	
	@After
	public void takeScreenshotOnFailure(Scenario scenario) {
		if(scenario.isFailed()) {
			byte[] src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
			scenario.attach(src, "image/png", scenario.getName());
		}
	}
	
	@AfterAll
	public static void tearDown() {
		driver.quit();
		driverManager = null;
	}
}
