package com.epam.stepdefs;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;

import static io.restassured.RestAssured.*;
import static org.testng.Assert.assertEquals;

import java.io.IOException;

import static com.epam.constants.ConstantProperties.*;

import com.epam.utility.FileReader;

public class LoginValidationStepDefs {
	public static final String USERCREATION_ENDPOINT = "Account/v1/User";
	Base base;
	
	public LoginValidationStepDefs(Base base) {
		this.base = base;
	}

	@When("the user add the credentials for Login")
	public void the_user_add_the_credentials_for_login() throws IOException {
		UserPojo userPojo = setCredentialsToUserPojo();
		 base.response = given()
				 		.contentType(ContentType.JSON)
				 		.body(userPojo)
				 		.when()
				 		.post(USERCREATION_ENDPOINT);
	}

	@Then("the status code should be {int}")
	public void the_status_code_should_be(int expectedStatusCode) {
		assertEquals(base.response.getStatusCode(), expectedStatusCode);
	}

	@Then("the username should be same as the user added in credentials")
	public void the_username_should_be_same_as_the_user_added_in_credentials() throws IOException {
		String expectedUsername = FileReader.readPropertiesFile(CREDENTIALS_PATH).getProperty("username");
		UserPojoResponse userPojoResponse = base.response.as(UserPojoResponse.class);
		assertEquals(userPojoResponse.getUsername(), expectedUsername);
	}
	
	private UserPojo setCredentialsToUserPojo() throws IOException {
		String username = FileReader.readPropertiesFile(CREDENTIALS_PATH).getProperty("username");
		String password = FileReader.readPropertiesFile(CREDENTIALS_PATH).getProperty("password");
		UserPojo userPojo = new UserPojo();
		userPojo.setUserName(username);
		userPojo.setPassword(password);
		return userPojo;
	}
	
	@Given("the user navigates to the Demoqa Login Page")
	public void the_user_navigates_to_the_book_store_application() {
		base.getLoginPageObject().openLoginPage();
	}
	
	@When("the user enter the credentials in username and password field")
	public void the_user_enter_the_credentials_in_username_and_password_field() throws IOException {
		base.getLoginPageObject().scrollDownTheLoginPage()
								 .enterUserName(FileReader.readPropertiesFile(CREDENTIALS_PATH).getProperty("username"))
								 .enterPassword(FileReader.readPropertiesFile(CREDENTIALS_PATH).getProperty("password"));
	}

	@When("the user clicks on Login button")
	public void the_user_clicks_on_login_button() {
		base.getLoginPageObject().clickOnLoginButton();
	}

	@Then("the user navigates to Profile Page with title {string}")
	public void the_user_navigates_to_profile_page_with_title(String expectedTitle) {
		assertEquals(base.getProfilePageObject().getTitleOfThePage(),expectedTitle);
	}

	@Then("the User Name should be the same as the user added in credentials")
	public void the_user_name_should_be_the_same_as_the_user_added_in_credentials() throws IOException {
		String expectedUsername = FileReader.readPropertiesFile(CREDENTIALS_PATH).getProperty("username");
		assertEquals(base.getProfilePageObject().getUsernameInProfilePage(), expectedUsername);
	}
}
