Feature: Validate the details of the books in the BookStore

	@BooksListValidation
  Scenario: Get the details of the books in the bookstore and validate them
    When the user gets the details of the books
    And the user navigates to Books Page
    Then the titles should be same as in the response body
    And the authors should be same as in the response body
    And the publishers should be same as in the response body