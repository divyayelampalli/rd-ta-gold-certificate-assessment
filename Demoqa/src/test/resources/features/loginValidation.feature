Feature: To create an account of the user in Books Webservice

	@LoginValidation
	Scenario: Passing credentials to create a user
	  When the user add the credentials for Login
	  Then the status code should be 201
	  And the username should be same as the user added in credentials
	    
	@LoginValidation
	Scenario: verifying the created user
		Given the user navigates to the Demoqa Login Page
		When the user enter the credentials in username and password field
		And the user clicks on Login button
		Then the user navigates to Profile Page with title "DEMOQA"
		And the User Name should be the same as the user added in credentials