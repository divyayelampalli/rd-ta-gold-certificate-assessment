package com.epam.exceptions;

@SuppressWarnings("serial")
public class BrowserNotFoundException extends RuntimeException {
	
	public BrowserNotFoundException(String message) {
		super(message);
	}
}
