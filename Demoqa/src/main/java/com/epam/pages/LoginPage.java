package com.epam.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage extends AbstractPage {
	public static final String BASE_URL = "https://demoqa.com/login";
	
	@FindBy(id = "userName")
	private WebElement userNameField;
	
	@FindBy(id = "password")
	private WebElement passwordField;
	
	@FindBy(id = "login")
	private WebElement loginButton;
	
	public LoginPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(this.driver, this);
	}

	public LoginPage openLoginPage() {
		driver.get(BASE_URL);
		driver.manage().window().maximize();
		return this;
	}
	
	public LoginPage scrollDownTheLoginPage() {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,250)", "");
		return this;
	}
	public LoginPage enterUserName(String userName) {
		actions = new Actions(driver);
		actions.moveToElement(userNameField).click().sendKeys(userName).build().perform();
		return this;
	}
	
	public LoginPage enterPassword(String password) {
		actions = new Actions(driver);
		actions.moveToElement(passwordField).click().sendKeys(password).build().perform();
		return this;
	}
	
	public ProfilePage clickOnLoginButton() {
		actions = new Actions(driver);
		actions.moveToElement(loginButton).click().build().perform();
		return new ProfilePage(driver);
	}

}
