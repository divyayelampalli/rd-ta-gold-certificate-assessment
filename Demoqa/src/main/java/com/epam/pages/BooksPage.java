package com.epam.pages;

import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BooksPage extends AbstractPage {
	public static final String BOOKSPAGE_URL = "https://demoqa.com/books";
	
	@FindBy(css = ".mr-2")
	private List<WebElement> titlesOfTheBooks;
	
	@FindBy(xpath = "//*[@class='rt-tr-group']/div/div/div/following::div[1]")
	private List<WebElement> authorsOfBooks;
	
	@FindBy(xpath = "//*[@class='rt-tr-group']/div/div/div/following::div[2]")
	private List<WebElement> publishersOfBooks;
	
	public BooksPage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(this.driver, this);
	}
	
	public BooksPage openBooksPage() {
		driver.get(BOOKSPAGE_URL);
		driver.manage().window().maximize();
		return this;
	}
	
	public List<String> getTilesOfBooksList(){
		return titlesOfTheBooks.stream().map(WebElement::getText).collect(Collectors.toList());
	}
	
	public List<String> getAuthorsOfBooksList(){
		return authorsOfBooks.stream().map(WebElement::getText).collect(Collectors.toList());
	}
	
	public List<String> getPublishersOfBooksList(){
		return publishersOfBooks.stream().map(WebElement::getText).collect(Collectors.toList());
	}
	
}
