package com.epam.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProfilePage extends AbstractPage {
	@FindBy(id = "userName-value")
	private WebElement userName;
	
	public ProfilePage(WebDriver driver) {
		super(driver);
		PageFactory.initElements(this.driver, this);
	}
	
	public String getTitleOfThePage() {
		return driver.getTitle();
	}
	
	public String getUsernameInProfilePage() {
		waitUntilElementVisible(userName);
		return userName.getText();
	}
}
