package com.epam.pages;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.epam.constants.ConstantProperties.*;

public class AbstractPage {
	protected WebDriver driver;
	protected Actions actions;
	
	public AbstractPage(WebDriver driver) {
		this.driver = driver;
	}
	
	public void waitUntilElementVisible(WebElement webElement) {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(WAIT_TIMEOUT_INSEC));
		wait.until(ExpectedConditions.visibilityOf(webElement));
	}
}
