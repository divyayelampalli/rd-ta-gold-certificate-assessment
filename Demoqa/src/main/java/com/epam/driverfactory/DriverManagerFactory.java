package com.epam.driverfactory;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.epam.exceptions.BrowserNotFoundException;
import com.epam.utility.FileReader;

import static com.epam.constants.ConstantProperties.*;

public class DriverManagerFactory {
	
	private static final Logger LOGGER = LogManager.getRootLogger();

	private static DriverManager driverManager;

	public static DriverManager getManager() throws IOException {

		if (driverManager == null) {
			switch (FileReader.readPropertiesFile(BROWSER_PATH).getProperty("browser").toUpperCase()) {
			case "CHROME":
				driverManager = new ChromeDriverManager();
				break;

			case "EDGE":
				driverManager = new EdgeDriverManager();
				break;
				
			case "FIREFOX":
				driverManager = new FirefoxDriverManager();
				break;

			default:
				LOGGER.error("Valid Brower is not provided");
				throw new BrowserNotFoundException("Please check and launch the correct browser");
			}
		}
		return driverManager;
	}
}
