package com.epam.constants;

public class ConstantProperties {
	
	public static final String BROWSER_PATH = "src/test/resources/browser.properties";
	
	public static final String CREDENTIALS_PATH = "src/test/resources/credentials.properties";
	
	public static final long WAIT_TIMEOUT_INSEC = 10;
}
