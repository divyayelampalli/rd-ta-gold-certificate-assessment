package com.epam.utility;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class FileReader {
	
	public static Properties readPropertiesFile(String fileName) throws IOException {
		Properties properties;
		try(FileInputStream fis = new FileInputStream(fileName)){
			properties = new Properties();
			properties.load(fis);
		}
		return properties;
	}
}
